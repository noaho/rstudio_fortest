adult<-read.csv(url('https://raw.githubusercontent.com/Geoyi/Salary-prediction/master/adult_sal.csv'))
str(adult)

summary(adult)

levels(adult$type_employer)

table(adult$type_employer)

unemp <- function(job){
  job <- as.character(job)
  if(job == 'Never-worked' | job == 'Without-pay'){
    return('Unemployed')
  }else{
    return(job)
  }
}

adult$type_employer <- sapply(adult$type_employer,unemp)

table(adult$type_employer)

str(adult)
#x is the index - we don't need it so we can get rid of it.
install.packages('dplyr')#for select
library(dplyr)
adult <- select(adult,-x)


#let reduce the job types:
group_emp <- function(job){
  if(job == 'Local-gov' | job == 'State-gov' | job == 'Federal-gov' ){
    return('Gov')
  }else if(job == 'Self-emp-inc' | job == 'Self-emp-not-inc'){
    return('Self-emp')
  } else {
    return(job)
  }
}
adult$type_employer <- sapply(adult$type_employer,group_emp)



table(adult$marital)

group_marital <- function(mar){
  mar <- as.character(mar)
  if(mar == 'Separated' | mar == 'Divorced' | mar == 'Widowed'){
    return('Not-married')
  }else if(mar == 'Never-married'){
    return(mar)
  } else {
    return('Married')
  }
}

adult$marital <- sapply(adult$marital ,group_marital )


table(adult$marital)

levels(adult$country)

Europe <- c("England","Greece", "Hungary", "Ireland", "Holand-Netherlands",
            "Poland" , "Italy","Scotland", "Yugoslavia", "Germany" , "France",
            "Portugal")


Asia <- c("China" ,"Japan" , "Hong", "Cambodia", "Philippines",  "Iran", 
          "Taiwan", "Thailand" ,  "Laos", "India",  "Vietnam"    )


North.america <- c("United-States" ,  "Canada", "Puerto-Rico"  ) 

Latin.and.south.america <- c("Columbia", "Cuba", "Dominican-Republic",
                             "Ecuador", "El-Salvador",  "Guatemala", "Haiti" ,
                             "Honduras", "Jamaica", "Nicaragua", "Mexico","Peru",
                             "Outlying-US(Guam-USVI-etc)" ,"Trinadad&Tobago" )
Other <- c("South")

group_country <- function(ctry){
  if(ctry %in%  Europe) 
  {return("Europe")} 
  else if (ctry %in% Asia)
  {return("Asia")}
  else if (ctry %in% North.america)
  {return("North.america")}
  else if (ctry %in% Latin.and.south.america)
  {return("Latin.and.south.america")}
  else 
  {return('Other')} 
}

adult$country <- sapply(adult$country,group_country )

table(adult$country)
#after we reduce the levels we must change to factor:
adult$type_employer <- factor(adult$type_employer)
adult$country <- factor(adult$country)
adult$marital  <- factor(adult$marital)

str(adult)
#we must get rid of the ? in our Data set:
#for all data in the data set
adult[adult=='?'] <- NA

table(adult$type_employer)
#inorder to find the NA we will use Amelia
install.packages("Amelia")
library(Amelia)
missmap(adult)#occpation and type_employer hav NA's
table(adult$occupation)

adult <- na.omit(adult)
str(adult)
library(dplyr)

install.packages("ggplot2")
library(ggplot2)
ggplot(adult, aes(age)) + geom_histogram(aes(fill = income),color= 'black', binwidth = 1)


levels(adult$income)
#cange the country lable to region:
adult <- rename(adult, region = country)

str(adult)


#now we split the data set
install.packages('caTools')
library(caTools)
set.seed(101)
sample <- sample.split(adult$income, SplitRatio = 0.7)

adult.train <- subset(adult, sample ==T)
adult.test <- subset(adult, sample ==F)

model.income <- glm(income ~ ., family = binomial(link = 'logit'), data = adult.train)

summary(model.income)
#-----------------------------
step.model.income <- step(model.income)

summary(step.model.income)
#-----------------------------

#predict:
predicted.test.income <- predict(model.income, newdata = adult.test, type = 'response')

#confusion
cm<-table(adult.test$income,predicted.test.income>0.5)
