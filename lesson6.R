titanic<-read.csv("titanic.csv")
install.packages("dplyr")
install.packages("Amelia")
library(dplyr)
library(Amelia)
str(titanic)


missmap(titanic, main = "Missing Data", col = c('yellow', 'black'))
#find all the NA data in age col
ageMissing <-is.na(titanic$Age)

meanAge <- mean(titanic$Age [!ageMissing])


imputeAge <-function(age){
  if (is.na(age)) {
    return(meanAge)
  }
  else{
    return(age)
  }
}

#now we will implement the imputation
titanic$Age <-sapply(titanic$Age, imputeAge)
library(ggplot2)
ggplot(titanic, aes(Survived))+geom_bar()

ggplot(titanic,aes(Age, fill=factor(Survived)))+geom_histogram(binwidth = 20)


ggplot(titanic,aes(Fare, fill=factor(Survived)))+geom_histogram(binwidth = 50)

#get rid of the a few col's in the data set
titanic.clean <-select(titanic,-PassengerId, -Name, -Ticket , -Cabin)
str(titanic.clean)
#chane survived & Pclass to vectors so later we can change to dummy's
titanic.clean$Survived <-factor(titanic.clean$Survived)
titanic.clean$Pclass<-factor(titanic.clean$Pclass)

#?????????? ???? train & test set
titanic.clean.train <-sample_frac(titanic.clean, 0.7)
sid <-as.numeric(rownames(titanic.clean.train))
#now we set the test:
titanic.clean.test<-titanic.clean[-sid,]


#start the model

log.model<-glm(Survived ~ ., family = binomial(link = 'logit'),titanic.clean.train)
#?????????? ??????????
summary(log.model)
#predict
predicted.probabilites<-predict(log.model, titanic.clean.test, type = 'response')
predicted.valuse <-ifelse(predicted.probabilites>0.5,1,0)
misClassError <-mean(predicted.valuse != titanic.clean.test$Survived)
